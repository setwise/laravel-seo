<?php

namespace Setwise\Seo\Traits;

use Setwise\Seo\Seo;

trait HasSeo
{
    /**
     * Return an array of validation rules used to validate seo parameters
     *
     * @return array
     */
    public static function SeoRules()
    {
        return [
            Seo::TitleField() => [
                'sometimes',
                'nullable',
                'string',
                'max:255',
            ],
            Seo::AuthorField() => [
                'sometimes',
                'nullable',
                'string',
                'max:255',
            ],
            Seo::DescriptionField() => [
                'sometimes',
                'nullable',
                'string',
                'max:512',
            ],
            Seo::KeywordsField() => [
                'sometimes',
                'array',
                'min:0',
                'max:5',
            ],
            Seo::ImageField() => [
                'sometimes',
                'nullable',
                'max:255',
            ],
        ];
    }

    /**
     * This method is called upon instantiation of the Eloquent Model.
     *
     * @return void
     */
    public function initializeHasSeo()
    {
        $this->mergeCasts([
            Seo::KeywordsField() => 'array',
        ]);
    }
}
