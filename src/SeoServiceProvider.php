<?php

namespace Setwise\Seo;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;
use Setwise\Seo\Database\Schema\Blueprint as BlueprintMacros;

class SeoServiceProvider extends ServiceProvider
{
    /** @var array */
    protected $blueprintMacros = [
        'applySeo' => BlueprintMacros\ApplySeoMacro::class,
    ];

    /**
     * Bootstrap the application services
     */
    public function boot()
    {
        // Load package assets
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'seo');

        if ($this->app->runningInConsole()) {
            // Publishing the config
            $this->publishes([
                __DIR__ . '/../config/seo.php' => config_path('seo.php'),
            ], 'setwise-seo-config');

            // Publishing the views
            $this->publishes([
                __DIR__ . '/../resources/views' => resource_path('views/vendor/seo'),
            ], 'setwise-seo-views');
        }

        // Register the application singleton
        $this->app->singleton('seo', function () {
            return new Seo();
        });
    }

    /**
     * Register the application services
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__ . '/../config/seo.php', 'seo');

        $this->registerBlueprintMacros();
    }

    /**
     * Bootstrap the blueprint macros
     *
     * @return void
     */
    protected function registerBlueprintMacros()
    {
        Collection::make($this->blueprintMacros)
            ->reject(fn($class, $macro) => Blueprint::hasMacro($macro))
            ->each(fn($class, $macro) => Blueprint::macro($macro, app($class)()));
    }
}
