<?php

namespace Setwise\Seo\Database\Schema\Blueprint;

use Setwise\Seo\Seo;

/**
 * Apply seo parameters to a given migration blueprint
 *
 * @mixin \Illuminate\Database\Schema\Blueprint
 *
 * @return $this
 */
class ApplySeoMacro
{
    public function __invoke() {
        return function () {
            $this->string(Seo::TitleField())
                ->nullable();

            $this->string(Seo::AuthorField())
                ->nullable();

            $this->text(Seo::DescriptionField())
                ->nullable();

            $this->json(Seo::KeywordsField())
                ->nullable();

            $this->string(Seo::ImageField())
                ->nullable();

            return $this;
        };
    }
}
