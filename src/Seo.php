<?php

namespace Setwise\Seo;

use Illuminate\Database\Eloquent\Model;
use Setwise\Seo\Traits\HasSeo;

class Seo
{
    /** @var \Illuminate\Database\Eloquent\Model|\Setwise\Seo\Traits\HasSeo */
    private $seoModel = null;

    /**
     * @return string
     */
    public static function TitleField()
    {
        return config('seo.database.title', 'seo_title');
    }

    /**
     * @return string
     */
    public static function AuthorField()
    {
        return config('seo.database.author', 'seo_author');
    }

    /**
     * @return string
     */
    public static function DescriptionField()
    {
        return config('seo.database.description', 'seo_description');
    }

    /**
     * @return string
     */
    public static function KeywordsField()
    {
        return config('seo.database.keywords', 'seo_keywords');
    }

    /**
     * @return string
     */
    public static function ImageField()
    {
        return config('seo.database.image', 'seo_image');
    }

    /**
     * Set the given seo model to be displayed on the views, if they are rendered
     *
     * @param \Illuminate\Database\Eloquent\Model|\Setwise\Seo\Traits\HasSeo $model
     */
    public function setSeoModel(Model $model)
    {
        if (!in_array(HasSeo::class, class_uses($model))) {
            abort(500, "Model {$model->getTable()} must implement HasSeo to be used as the SEO model");
        }

        $this->seoModel = $model;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|\Setwise\Seo\Traits\HasSeo
     */
    public function seoModel()
    {
        return $this->seoModel;
    }

    /**
     * Return the current seo title
     *
     * @return string
     */
    public function getSeoTitle()
    {
        return $this->seoModel->{self::TitleField()} ?? config('seo.defaults.title', '');
    }

    /**
     * Return the current seo author
     *
     * @return string
     */
    public function getSeoAuthor()
    {
        return $this->seoModel->{self::AuthorField()} ?? config('seo.defaults.author', '');
    }

    /**
     * Return the current seo description
     *
     * @return string
     */
    public function getSeoDescription()
    {
        return $this->seoModel->{self::DescriptionField()} ?? config('seo.defaults.description', '');
    }

    /**
     * Return the current seo keywords
     *
     * @return string
     */
    public function getSeoKeywords()
    {
        $keywords = $this->seoModel->{self::KeywordsField()} ?? config('seo.defaults.keywords', []);

        return implode(',', $keywords);
    }

    /**
     * Return the current seo image
     *
     * @return string
     */
    public function getSeoImage()
    {
        return $this->seoModel->{self::ImageField()} ?? config('seo.defaults.image', '');
    }
}
