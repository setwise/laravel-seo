<?php

namespace Setwise\Seo;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Setwise\Seo\Seo
 */
class SeoFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'seo';
    }
}
