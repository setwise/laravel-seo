# Setwise Seo  
  
This package aims to consolidate all Setwise seo generation into a single, cohesive package.  
  
### Installation  
  
Install via the command  
```bash  
composer require setwise/laravel-seo  
```  
  
### Configuration  
  
To publish this package's config file use the command   
```bash  
php artisan vendor:publish --tag=setwise-seo-config  
```  
  
### Form Components  
  
This package works to provide standard seo components.
These blade files can be customized by publishing them to your application using the command:   
```bash  
php artisan vendor:publish --tag=setwise-seo-views  
```  
  
---

### Seo Headings
To apply seo meta tags to your blade file simply put `@include(seo::headings)` within your `<head></head>` tags.

The `headings` blade file pulls its information from the Seo facade created by the application.  
If you wish to use a model to populate these fields rather than using the config driven defaults, 
simply put the line `Seo::setSeoModel($model)` before the heading is rendered, and the model's seo fields will be populated into the page

---
 
### Seo Field
Because SEO can be applied to many models, a few helper components were created to display the fields  
  
#### Display Fields
```blade
 @include('seo::rows', [
    'model' => $page,
])
```

#### Form Fields
```blade
@include('seo::form', [
    'seoImageUrl' => route(''),
    'seoImageDeleteUrl' => route(''),
])
```

---

### Testing  
  
``` bash  
composer test  
```  
  
### Changelog  
  
Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.  
  
## Credits  
  
- [Nicholas Dykhuizen](https://bitbucket.com/setwise)