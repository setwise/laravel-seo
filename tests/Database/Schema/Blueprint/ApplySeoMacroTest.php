<?php

namespace Setwise\Seo\Tests\Unit\Database\Schema\Blueprint;

use Illuminate\Database\Schema\Blueprint;
use Setwise\Seo\Tests\TestCase;

class ApplySeoMacroTest extends TestCase {

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function testApplySeo() {
        $blueprint = app()->make(Blueprint::class, ['table' => 'my_table']);
        $blueprint->applySeo();

        $this->assertCount(5, $blueprint->getColumns());
    }
}
