<?php

namespace Setwise\Seo\Tests;

use Orchestra\Testbench\TestCase as TestBench;
use Setwise\Seo\SeoFacade;
use Setwise\Seo\SeoServiceProvider;

abstract class TestCase extends TestBench
{
    protected function getPackageProviders($app)
    {
        return [SeoServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [SeoFacade::class];
    }
}
