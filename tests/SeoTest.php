<?php

namespace Setwise\Seo\Tests;

use Setwise\Seo\Seo;
use Setwise\Seo\SeoFacade;

class SeoTest extends TestCase
{
    public function testFacade()
    {
        $this->assertNull(SeoFacade::seoModel());
    }

    public function testStaticGetters()
    {
        $this->assertEquals('seo_title', Seo::TitleField());
        $this->assertEquals('seo_author', Seo::AuthorField());
        $this->assertEquals('seo_description', Seo::DescriptionField());
        $this->assertEquals('seo_keywords', Seo::KeywordsField());
        $this->assertEquals('seo_image', Seo::ImageField());
    }

    public function testSeoGetters()
    {
        $this->assertEquals(config('seo.defaults.title'), SeoFacade::getSeoTitle());
        $this->assertEquals(config('seo.defaults.author'), SeoFacade::getSeoAuthor());
        $this->assertEquals(config('seo.defaults.description'), SeoFacade::getSeoDescription());
        $this->assertEquals(implode(',', config('seo.defaults.keywords')), SeoFacade::getSeoKeywords());
        $this->assertEquals(config('seo.defaults.image'), SeoFacade::getSeoImage());

        $model = HasSeoModel::make([
            'seo_title' => 'TestModel',
            'seo_author' => 'Nick',
            'seo_description' => 'Description',
            'seo_keywords' => ['Hello', 'World'],
            'seo_image' => 'https://google.com',
        ]);
        SeoFacade::setSeoModel($model);

        $this->assertEquals('TestModel', SeoFacade::getSeoTitle());
        $this->assertEquals('Nick', SeoFacade::getSeoAuthor());
        $this->assertEquals('Description', SeoFacade::getSeoDescription());
        $this->assertEquals('Hello,World', SeoFacade::getSeoKeywords());
        $this->assertEquals('https://google.com', SeoFacade::getSeoImage());
    }
}
