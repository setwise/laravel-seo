<?php

namespace Setwise\Seo\Tests\Unit\Traits;

use Setwise\Seo\Tests\HasSeoModel;
use Setwise\Seo\Tests\TestCase;

class HasSeoTest extends TestCase
{
    public function testInitialize()
    {
        $model = HasSeoModel::make();

        $this->assertArrayHasKey('seo_keywords', $model->getCasts());
    }

    public function testRules()
    {
        $this->assertIsArray(HasSeoModel::SeoRules());
    }
}
