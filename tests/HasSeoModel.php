<?php

namespace Setwise\Seo\Tests;

use Illuminate\Database\Eloquent\Model;
use Setwise\Seo\Traits\HasSeo;

/**
 * @mixin \Eloquent
 */
class HasSeoModel extends Model
{
    use HasSeo;

    /** @var array */
    protected $guarded = [];
}
