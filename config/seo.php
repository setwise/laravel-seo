<?php

return [

    /**
     * These fields represent the default seo parameters on the application
     */
    'defaults' => [
        'name' => env('APP_NAME', 'Setwise'),
        'author' => 'Setwise Technology, LLC',
        'title' => env('APP_NAME', 'Setwise'),
        'description' => 'This is a skeleton application site for structural purposes',
        'keywords' => [
            'setwise',
            'skeleton',
            'website'
        ],
        'image' => '',
    ],

    /**
     * These values represent the field names given on models when using blueprints and models
     */
    'database' => [
        'title' => 'seo_title',
        'author' => 'seo_author',
        'description' => 'seo_description',
        'keywords' => 'seo_keywords',
        'image' => 'seo_image',
    ],
];
