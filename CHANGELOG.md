# Changelog
All notable changes to `seo` will be documented in this file

## Development

## 1.0.1 - July 29th, 2020
- Packagist open sourced on packagist

## 1.0.0 - July 9th, 2020
- Initial release
