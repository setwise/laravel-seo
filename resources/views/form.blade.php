<x-input.text
        name="{{ Seo::TitleField() }}"
>
</x-input.text>

<x-input.text
        name="{{ Seo::AuthorField() }}"
>
</x-input.text>


<x-input.text
        name="{{ Seo::DescriptionField() }}"
>
</x-input.text>

<x-input.select
        name="{{ Seo::KeywordsField() }}"
        :options="[]"
        :multiple="true"
        :taggable="true"
>
</x-input.select>

@if(isset($seoImageUrl) && isset($seoImageDeleteUrl))
    <x-input.upload.image
            name="{{ Seo::ImageField() }}"
            url="{{ $seoImageUrl }}"
            delete-url="{{ $seoImageDeleteUrl }}"
    >
    </x-input.upload.image>
@endif
