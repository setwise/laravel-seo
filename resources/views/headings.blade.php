{{-- Generic Meta Tags --}}
<meta name="title" content="{{ Seo::getSeoTitle() }}">
<meta name="author" content="{{ Seo::getSeoAuthor() }}">
<meta name="description" content="{{ Seo::getSeoDescription() }}">
<meta name="keywords" content="{{ Seo::getSeoKeywords() }}">
<meta name="canonical" content="{{ Request::fullUrl() }}">

{{-- Open Graph data --}}
{{-- Dynamic Fields --}}
<meta property="og:title" content="{{ Seo::getSeoTitle() }}"/>
<meta property="og:description" content="{{ Seo::getSeoDescription() }}"/>
@if(Seo::getSeoImage())
    <meta property="og:image" content="{{ Seo::getSeoImage() }}"/>
@endif

{{-- Static Fields --}}
<meta property="og:url" content="{{ request()->fullUrl() }}"/>
<meta property="og:locale" content="en_US"/>
<meta property="og:site_name" content="{{ Config::get('seo.defaults.name', 'Setwise') }}"/>
<meta property="og:type" content="website"/>

{{-- Twitter Card data --}}
{{-- Dynamic Fields --}}
<meta name="twitter:title" content="{{ Seo::getSeoTitle() }}">
<meta name="twitter:description" content="{{ Seo::getSeoDescription() }}">
<meta name="twitter:creator" content="{{Seo::getSeoAuthor() }}">
@if(Seo::getSeoImage())
    <meta property="twitter:image" content="{{ Seo::getSeoImage() }}"/>
@endif

{{-- Static Fields --}}
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="">
