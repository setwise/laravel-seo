<x-model.row label="Seo Title" :orientation="$orientation ?? 'horizontal'">
    {{ $model->{Seo::TitleField()} ?: 'Null' }}
</x-model.row>

<x-model.row label="Seo Author" :orientation="$orientation ?? 'horizontal'">
    {{ $model->{Seo::AuthorField()} ?: 'Null' }}
</x-model.row>

<x-model.row label="Seo Description" :orientation="$orientation ?? 'horizontal'">
    {{ $model->{Seo::DescriptionField()} ?: 'Null' }}
</x-model.row>

<x-model.row label="Seo Keywords" :orientation="$orientation ?? 'horizontal'">
    {{ implode(', ', $model->{Seo::KeywordsField()} ?: ['Null']) }}
</x-model.row>

<x-model.row label="Seo Image" :orientation="$orientation ?? 'horizontal'">
    @isset($model->{Seo::ImageField()})
        @if($model->{Seo::ImageField()})
            <div class="flex flex-col">
                <div>
                    <img class="h-auto max-w-full" src="{{ $model->{Seo::ImageField()} }}" alt="Seo Image">
                </div>
            </div>
        @else
            Null
        @endif
    @endisset
</x-model.row>
